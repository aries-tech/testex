package main_test

import (
	"database/sql"
	"log"
	"testing"

	_ "github.com/lib/pq"
)

const (
	dbUser     = "postgres"
	dbPassword = "postgres"
	dbName     = "events_test"
	dbHost     = "postgres"
	dbPort     = "5432"
)

func TestPostgresDBConnection(t *testing.T) {
	// Create the database connection string
	connStr := "postgres://" + dbUser + ":" + dbPassword + "@" + dbHost + ":" + dbPort + "/" + dbName + "?sslmode=disable"

	// Attempt to connect to the PostgreSQL database
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatalf("Failed to connect to the database: %v", err)
	}

	// Check if the connection is successful
	err = db.Ping()
	if err != nil {
		t.Fatalf("Failed to ping the database: %v", err)
	}

	// Close the database connection
	err = db.Close()
	if err != nil {
		log.Fatalf("Failed to close the database connection: %v", err)
	}
}
